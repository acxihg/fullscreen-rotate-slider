Fullscreen-rotate-slider
========================

A fullscreen slider plugin with indicator control

Touch swipe plugin from https://github.com/mattbryson/TouchSwipe-Jquery-Plugin  
Easing from: http://gsgd.co.uk/sandbox/jquery/easing/

How to use:

1. First, put the slider content as a list in a div with id key_visual, like

```
#!html

<div id="key_visual">
  	<ul class="clearfix">
		  <li id="key_visual_slider01"></li>
		  <li id="key_visual_slider02"></li>
		  <li id="key_visual_slider03"></li>
  	</ul>
</div>
```

2. Second, put the indicator as a list in a div, like

```
#!html
<div id="main_slider" class="clearfix">
	<li>
		<div>
		 <ul id="slider_dot_indicator" class="clearfix">
				<li data-id="#key_visual_slider01" class="active fast_transition"></li>
				<li data-id="#key_visual_slider02" class="fast_transition"></li>
				<li data-id="#key_visual_slider03" class="fast_transition"></li>
			</ul>
		</div>
	</li>
</ul>

```


3. And then called rotateSlider()

```
#!javascript

$('#key_visual').rotateSlider();

```

(4.) Adjust the plugin
slideshowTimer - The time on slide goes to another slide while slideshow is true
slideshow  - If using automatic slideshow or not
indicatorListId - id of the indicator container
duration - Transition of slides after click on the indicator

Ex.

```
#!javascript

$('#key_visual').rotateSlider({slideshow: false});
```